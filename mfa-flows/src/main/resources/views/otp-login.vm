##
## Velocity Template for DisplayUsernamePasswordPage view-state
##
## Velocity context will contain the following properties
## flowExecutionUrl - the form action location
## flowRequestContext - the Spring Web Flow RequestContext
## flowExecutionKey - the SWF execution key (this is built into the flowExecutionUrl)
## profileRequestContext - root of context tree
## authenticationContext - context with authentication request information
## authenticationErrorContext - context with login error state
## authenticationWarningContext - context with login warning state
## ldapResponseContext - context with LDAP state (if using native LDAP)
## rpUIContext - the context with SP UI information from the metadata
## extendedAuthenticationFlows - collection of "extended" AuthenticationFlowDescriptor objects
## encoder - HTMLEncoder class
## request - HttpServletRequest
## response - HttpServletResponse
## environment - Spring Environment object for property resolution
## custom - arbitrary object injected by deployer
##
#set ($rpContext = $profileRequestContext.getSubcontext('net.shibboleth.idp.profile.context.RelyingPartyContext'))
#set ($username = $authenticationContext.getSubcontext('net.shibboleth.idp.authn.context.UsernamePasswordContext', true).getUsername())
#set ($event = $profileRequestContext.getSubcontext('org.opensaml.profile.context.EventContext').getEvent())
##
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>#springMessageText("idp.title", "Web Login Service")</title>
    <link rel="stylesheet" type="text/css" href="$request.getContextPath()/css/main.css">
    <link rel="stylesheet" type="text/css" href="$request.getContextPath()/css/idphosting.css">
    <link rel="shortcut icon" href="$request.getContextPath()#springMessageText("idp.favicon", "/images/favicon.ico")" />
  </head>
  <body>
    <div class="aai_box">
      <div class="container">
        <a href="https://www.switch.ch/aai/">
          <img class="aai_logo" alt="SWITCHaai-Logo" src="$request.getContextPath()/images/switchaai-logo.png">
        </a>
        <a href="#springMessage("idp.logo.target.url")">
          <img align="right" style="border: 0" src="$request.getContextPath()#springMessage("idp.logo")" alt="#springMessageText("idp.logo.alt-text", "logo")">
        </a>
      </div>

      <div>
        <h1 class="aai">#springMessageText("idp.title", "Web Login Service")</h1>
        #if ($rpUIContext.serviceName)
        #set ($serviceName = $rpUIContext.serviceName)
        #else
        #set ($serviceName = $rpContext.relyingPartyId)
        #end
        <p>#springMessageText("idp.login.intro" [$serviceName])</p>
      </div>

      <div class="aai_login_field">
        <form action="$flowExecutionUrl" method="post">
          #parse("login-error.vm")
          #if ($event && $event == "sms_otp_sent")
          <section>
            <p class="login_error">#springMessageText("idp.login.sms_otp_sent", "SMS OTP sent")</p>
          </section>
          #end

          <div class="form-group">
            <label for="username">#springMessageText("idp.login.username", "Username")</label>
            <p class="form-element" id="username">#if($initialUsername)$encoder.encodeForHTML($initialUsername)#end</p>
          </div>

          <div class="form-group">
            <label for="otp">#springMessageText("idp.login.password", "Password")</label>
            <input class="form-element" id="otp" name="j_otp" type="text" placeholder="OTP" value="">
          </div>

          <div class="aai_login_button">
            <button class="aai_login_button" type="submit" name="_eventId_submit">#springMessageText("idp.login.login", "Login")</button>
          </div>
          <div class="aai_login_button">
            <button class="aai_login_button" type="submit" name="_eventId_requestsmsotp">#springMessageText("idp.login.requestsms", "Request SMS OTP")</button>
          </div>
        </form>
      </div>
    </div>
  </body>
</html>

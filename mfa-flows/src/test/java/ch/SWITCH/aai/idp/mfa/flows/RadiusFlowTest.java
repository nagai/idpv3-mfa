package ch.SWITCH.aai.idp.mfa.flows;

import static org.hamcrest.CoreMatchers.*;
import static org.junit.Assert.assertThat;

import java.util.HashSet;
import java.util.Set;

import javax.security.auth.Subject;

import net.shibboleth.idp.authn.AuthenticationFlowDescriptor;
import net.shibboleth.idp.authn.AuthenticationResult;
import net.shibboleth.idp.authn.context.AuthenticationContext;
import net.shibboleth.idp.authn.context.AuthenticationErrorContext;
import net.shibboleth.idp.authn.principal.UsernamePrincipal;
import net.shibboleth.idp.ui.context.RelyingPartyUIContext;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;
import org.opensaml.profile.context.EventContext;
import org.opensaml.profile.context.ProfileRequestContext;
import org.springframework.beans.factory.config.ConfigurableBeanFactory;
import org.springframework.mock.web.MockHttpServletRequest;
import org.springframework.webflow.config.FlowDefinitionResource;
import org.springframework.webflow.config.FlowDefinitionResourceFactory;
import org.springframework.webflow.core.collection.LocalAttributeMap;
import org.springframework.webflow.core.collection.MutableAttributeMap;
import org.springframework.webflow.execution.FlowExecution;
import org.springframework.webflow.test.MockExternalContext;
import org.springframework.webflow.test.MockFlowBuilderContext;
import org.springframework.webflow.test.execution.AbstractXmlFlowExecutionTests;

@RunWith(JUnit4.class)
public class RadiusFlowTest extends AbstractXmlFlowExecutionTests {

  private MockHttpServletRequest mockRequest = new MockHttpServletRequest();
  private OtpValidator mockOtpValidator = new AcceptingOtpValidator();

  @Override
  protected FlowDefinitionResource getResource(FlowDefinitionResourceFactory resourceFactory) {
    return resourceFactory.createFileResource("src/main/resources/flows/authn/radius/radius-flow.xml");
  }

  @Override
  protected FlowDefinitionResource[] getModelResources(FlowDefinitionResourceFactory resourceFactory) {
    return new FlowDefinitionResource[] {
        resourceFactory.createResource("/system/flows/authn/authn-abstract-flow.xml", null, "authn.abstract"),
        resourceFactory.createResource("/conf/authn/authn-events-flow.xml", null, "authn.events") };
  }

  @Override
  protected void configureFlowBuilderContext(MockFlowBuilderContext builderContext) {
    // Register a mock HttpServletRequest bean before flow beans are loaded.
    builderContext.registerBean("shibboleth.HttpServletRequest", mockRequest);
  }

  @Override
  protected void registerMockFlowBeans(ConfigurableBeanFactory flowBeanFactory) {
    // Overwrite the RadiusOtpValidator bean with a mock, after flow beans are
    // loaded.
    flowBeanFactory.registerSingleton("RadiusOtpValidator", mockOtpValidator);
  }

  @Test
  public void flowShouldStartAtView() {
    FlowExecution flowExecution = prepareFlowExecution(mockProfileRequestContext());
    MutableAttributeMap<Object> input = new LocalAttributeMap<Object>();
    input.put("calledAsSubflow", true);
    flowExecution.start(input, new MockExternalContext());
    assertFlowExecutionActive();
    assertCurrentStateEquals("DisplayOtpLoginForm");
  }

  @Test
  public void flowShouldSetViewAttributes() {
    FlowExecution flowExecution = prepareFlowExecution(mockProfileRequestContext());
    MutableAttributeMap<Object> input = new LocalAttributeMap<Object>();
    input.put("calledAsSubflow", true);
    MockExternalContext context = new MockExternalContext();
    flowExecution.start(input, context);
    assertThat(getRequiredViewAttribute("request"), is(sameInstance(context.getNativeRequest())));
    assertThat(getRequiredViewAttribute("profileRequestContext"), is(instanceOf(ProfileRequestContext.class)));
    assertThat(getRequiredViewAttribute("authenticationContext"), is(instanceOf(AuthenticationContext.class)));
    assertThat(getRequiredViewAttribute("encoder"), is(instanceOf(Class.class)));
    assertThat((String) getRequiredViewAttribute("initialUsername"), is(equalTo("initialuser")));
  }

  @Test
  public void formSubmitShouldTransitionToEnd() {
    mockRequest.addParameter("j_otp", "123456");
    FlowExecution flowExecution = prepareFlowExecution(mockProfileRequestContext());
    setCurrentState("DisplayOtpLoginForm");
    MockExternalContext context = new MockExternalContext();
    context.setEventId("submit");
    resumeFlow(context);
    assertFlowExecutionEnded();
    assertFlowExecutionOutcomeEquals("proceed");
  }

  @Test
  public void flowShouldCreateAuthenticationResult() {
    mockRequest.addParameter("j_otp", "123456");
    ProfileRequestContext<Object, Object> prc = mockProfileRequestContext();
    FlowExecution flowExecution = prepareFlowExecution(prc);
    MutableAttributeMap<Object> input = new LocalAttributeMap<Object>();
    input.put("calledAsSubflow", true);
    MockExternalContext context = new MockExternalContext();
    flowExecution.start(input, context);
    context.setEventId("submit");
    resumeFlow(context);
    AuthenticationResult ar = prc.getSubcontext(AuthenticationContext.class).getAuthenticationResult();
    assertThat(ar, is(notNullValue()));
    assertThat(ar.getSubject().getPrincipals(UsernamePrincipal.class), hasItem(new UsernamePrincipal("initialuser")));
  }

  @Test
  public void whenCalledWithoutInitialAuthentication_flowShouldCreateAuthenticationResult() {
    mockRequest.addParameter("j_otp", "123456");
    ProfileRequestContext<Object, Object> prc = mockProfileRequestContext();
    prc.getSubcontext(AuthenticationContext.class).setInitialAuthenticationResult(null);
    FlowExecution flowExecution = prepareFlowExecution(prc);
    MutableAttributeMap<Object> input = new LocalAttributeMap<Object>();
    input.put("calledAsSubflow", true);
    MockExternalContext context = new MockExternalContext();
    flowExecution.start(input, context);
    context.setEventId("submit");
    resumeFlow(context);
    AuthenticationResult ar = prc.getSubcontext(AuthenticationContext.class).getAuthenticationResult();
    assertThat(ar.getAuthenticationFlowId(), is(equalTo("authn/radius")));
    assertThat(ar.getSubject().getPrincipals(UsernamePrincipal.class), hasItem(new UsernamePrincipal("initialuser")));
  }

  @Test
  public void whenOtpEmpty_flowShouldDisplayFormAgain() {
    prepareFlowExecution(mockProfileRequestContext());
    setCurrentState("DisplayOtpLoginForm");
    MockExternalContext context = new MockExternalContext();
    context.setEventId("submit");
    resumeFlow(context);
    assertFlowExecutionActive();
    assertCurrentStateEquals("DisplayOtpLoginForm");
  }

  @Test
  public void whenOtpEmpty_flowShouldCreateAuthenticationErrorContext() {
    ProfileRequestContext<Object, Object> prc = mockProfileRequestContext();
    prepareFlowExecution(prc);
    setCurrentState("DisplayOtpLoginForm");
    MockExternalContext context = new MockExternalContext();
    context.setEventId("submit");
    resumeFlow(context);
    AuthenticationErrorContext aec = prc.getSubcontext(AuthenticationContext.class)
        .getSubcontext(AuthenticationErrorContext.class);
    assertThat(aec, is(notNullValue()));
    assertThat(aec.getClassifiedErrors(), hasItem("InvalidPassword"));
  }

  @Test
  public void whenOtpInvalid_flowShouldDisplayFormAgain() {
    mockOtpValidator = new RejectingOtpValidator();
    mockRequest.addParameter("j_otp", "invalid");
    prepareFlowExecution(mockProfileRequestContext());
    setCurrentState("DisplayOtpLoginForm");
    MockExternalContext context = new MockExternalContext();
    context.setEventId("submit");
    resumeFlow(context);
    assertFlowExecutionActive();
    assertCurrentStateEquals("DisplayOtpLoginForm");
  }

  @Test
  public void whenOtpInvalid_flowShouldCreateAuthenticationErrorContext() {
    mockOtpValidator = new RejectingOtpValidator();
    mockRequest.addParameter("j_otp", "invalid");
    ProfileRequestContext<Object, Object> prc = mockProfileRequestContext();
    prepareFlowExecution(prc);
    setCurrentState("DisplayOtpLoginForm");
    MockExternalContext context = new MockExternalContext();
    context.setEventId("submit");
    resumeFlow(context);
    AuthenticationErrorContext aec = prc.getSubcontext(AuthenticationContext.class)
        .getSubcontext(AuthenticationErrorContext.class);
    assertThat(aec, is(notNullValue()));
    assertThat(aec.getClassifiedErrors(), hasItem("InvalidPassword"));
  }

  @Test
  public void whenRequestSmsOtpEvent_flowShouldDisplayFormAgain() {
    prepareFlowExecution(mockProfileRequestContext());
    setCurrentState("DisplayOtpLoginForm");
    MockExternalContext context = new MockExternalContext();
    context.setEventId("requestsmsotp");
    resumeFlow(context);
    assertFlowExecutionActive();
    assertCurrentStateEquals("DisplayOtpLoginForm");
  }

  @Test
  public void whenRequestSmsOtpEvent_flowShouldCreateEventContext() {
    ProfileRequestContext<Object, Object> prc = mockProfileRequestContext();
    prepareFlowExecution(prc);
    setCurrentState("DisplayOtpLoginForm");
    MockExternalContext context = new MockExternalContext();
    context.setEventId("requestsmsotp");
    resumeFlow(context);
    EventContext<String> ec = prc.getSubcontext(EventContext.class);
    assertThat(ec, is(notNullValue()));
    assertThat(ec.getEvent(), is(equalTo("sms_otp_sent")));
  }

  @Test
  public void whenRequestSmsOtpEvent_flowShouldRemoveAuthenticationErrorContext() {
    ProfileRequestContext<Object, Object> prc = mockProfileRequestContext();
    prepareFlowExecution(prc);
    prc.getSubcontext(AuthenticationContext.class).addSubcontext(new AuthenticationErrorContext(), true);
    setCurrentState("DisplayOtpLoginForm");
    MockExternalContext context = new MockExternalContext();
    context.setEventId("requestsmsotp");
    resumeFlow(context);
    assertThat(prc.getSubcontext(AuthenticationContext.class).getSubcontext(AuthenticationErrorContext.class), is(nullValue()));
  }

  private FlowExecution prepareFlowExecution(ProfileRequestContext<Object, Object> prc) {
    FlowExecution flowExecution = getFlowExecutionFactory().createFlowExecution(getFlowDefinition());
    updateFlowExecution(flowExecution);
    flowExecution.getConversationScope().put(ProfileRequestContext.BINDING_KEY, prc);
    return flowExecution;
  }

  private ProfileRequestContext<Object, Object> mockProfileRequestContext() {
    ProfileRequestContext<Object, Object> prc = new ProfileRequestContext<Object, Object>();
    Subject initialSubject = new Subject();
    initialSubject.getPrincipals().add(new UsernamePrincipal("initialuser"));
    AuthenticationResult ar = new AuthenticationResult("authn/Password", initialSubject);
    AuthenticationContext ac = new AuthenticationContext();
    Set<AuthenticationResult> activeResults = new HashSet<AuthenticationResult>();
    activeResults.add(ar);
    ac.setActiveResults(activeResults);
    ac.setInitialAuthenticationResult(ar);
    AuthenticationFlowDescriptor afd = new AuthenticationFlowDescriptor();
    afd.setId("authn/radius");
    ac.setAttemptedFlow(afd);
    ac.addSubcontext(new RelyingPartyUIContext());
    prc.addSubcontext(ac);
    return prc;
  }
}
